import React from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Login from "./components/Login";
import SignUp from "./components/Signup";
import Home from "./components/Home";

function App() {


  return (<Router>
    <div className="App">
      <nav className="navbar navbar-expand-lg navbar-light fixed-top">
        <div className="container">
          <Link className="navbar-brand" to={"/sign-in"}>Prem Varma</Link>
          <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
            {localStorage.getItem('login') == 'true' ?
              < ul className="navbar-nav ml-auto">
                <li className="nav-item">
                  <Link className="nav-link" to={"/home"}>Home</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to={"/"}><span onClick={() => { localStorage.removeItem("login"); window.location.reload(); }}>Logout</span></Link>
                </li>
              </ul> :
              < ul className="navbar-nav ml-auto">
                <li className="nav-item">
                  <Link className="nav-link" to={"/sign-in"}>Sign In</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to={"/sign-up/new"}>Sign up</Link>
                </li>
              </ul>
            }
          </div>
        </div>
      </nav>

      <div className="auth-wrapper">
        <div className="auth-inner">
          <Switch>
            <Route exact path='/' component={Login} />
            <Route path="/sign-in" component={Login} />
            <Route path="/sign-up/:type" component={SignUp} />
            <Route path="/home" component={Home} />
          </Switch>
        </div>
      </div>
    </div></Router >
  );
}

export default App;