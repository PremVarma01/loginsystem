import React, { Component } from "react";
import Input from './Input';
import { Redirect, Link } from 'react-router-dom';

export default class Login extends Component {

    state = {
        controls: {
            username: {
                elementType: 'input',
                label: 'Username',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter username'
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Your Password'
                },
                label: 'Password',
                value: '',
                validation: {
                    required: true,
                    minLength: 5
                },
                valid: false,
                touched: false
            }
        },
    }

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = {
            ...this.state.controls,
            [controlName]: {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true
            }
        };
        this.setState({ controls: updatedControls });
    }

    submitHandler = (event) => {
        event.preventDefault();
        if (!this.state.controls.username.valid) {
            this.setState({ error_message: 'Please enter username' });
        }
        else if (!this.state.controls.password.valid) {
            this.setState({ error_message: 'Password should be of length 5' });
        }
        else {
            if (JSON.parse(localStorage.getItem('userData'))) {
                if (JSON.parse(localStorage.getItem('userData')).username === this.state.controls.username.value && JSON.parse(localStorage.getItem('userData')).password === this.state.controls.password.value) {
                    localStorage.setItem("login", true);
                    this.props.history.push('/home');
                    window.location.reload();

                } else {
                    this.setState({ error_message: 'Wrong ! Please check your email and password' });
                }
            } else {
                this.setState({ error_message: 'Ohh ! Sorry This user does not exist' });
            }
        }
    }



    render() {

        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = formElementsArray.map(formElement => (
            <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                label={formElement.config.label}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                changed={(event) => this.inputChangedHandler(event, formElement.id)} />
        ));


        let errorMessage = null;

        if (this.state.error_message) {
            errorMessage = (
                <p style={{ color: 'red' }}>{this.state.error_message}</p>
            );
        }

        return (
            <div>
                {errorMessage}
                <h3>Login</h3>
                <form onSubmit={this.submitHandler}>
                    {form}
                    <button type="submit" className="btn btn-primary btn-block">Submit</button>
                    <p className="forgot-password text-right">
                        New user? register here ! <Link to="/sign-up/new">sign up?</Link>
                    </p>
                </form>
            </div>
        );
    }

}