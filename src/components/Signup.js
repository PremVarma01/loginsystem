import React, { Component } from "react";
import Input from './Input'
import { Link } from "react-router-dom";

export default class SignUp extends Component {

    state = {
        error_message: null,
        controls: {
            name: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Your Name'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            contact: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter contact'
                },
                value: '',
                validation: {
                    required: true,
                    isNumeric: true,
                    minLength: 10,
                    maxLength: 10
                },
                valid: false,
                touched: false
            },
            email: {
                elementType: 'input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Your E-mail'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            username: {
                elementType: 'input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Enter username'
                },
                value: '',
                validation: {
                    required: true
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Your password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 5
                },
                valid: false,
                touched: false
            }
        }
    }

    componentDidMount() {
        if (this.props.match.params.type == 'edit' && localStorage.getItem('login') == 'true') {
            var userData = JSON.parse(localStorage.getItem("userData"));
            const updatedControls = {
                ...this.state.controls,
                ['name']: {
                    ...this.state.controls['name'],
                    value: userData['name'],
                    valid: true,
                    touched: true
                },
                ['email']: {
                    ...this.state.controls['email'],
                    value: userData['email'],
                    valid: true,
                    touched: true
                },
                ['password']: {
                    ...this.state.controls['password'],
                    value: userData['password'],
                    valid: true,
                    touched: true
                },
                ['username']: {
                    ...this.state.controls['username'],
                    value: userData['username'],
                    valid: true,
                    touched: true
                },
                ['contact']: {
                    ...this.state.controls['contact'],
                    value: userData['contact'],
                    valid: true,
                    touched: true
                },
            };
            this.setState({ controls: updatedControls });
        } else if (this.props.match.params.type == 'edit' && localStorage.getItem('login') !== 'true') {
            this.props.history.push('/');
        }

    }

    checkValidity(value, rules) {
        let isValid = true;
        if (!rules) {
            return true;
        }

        if (rules.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (rules.minLength) {
            isValid = value.length >= rules.minLength && isValid
        }

        if (rules.maxLength) {
            isValid = value.length <= rules.maxLength && isValid
        }

        if (rules.isEmail) {
            const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
            isValid = pattern.test(value) && isValid
        }

        if (rules.isNumeric) {
            const pattern = /^\d+$/;
            isValid = pattern.test(value) && isValid
        }

        return isValid;
    }

    inputChangedHandler = (event, controlName) => {
        const updatedControls = {
            ...this.state.controls,
            [controlName]: {
                ...this.state.controls[controlName],
                value: event.target.value,
                valid: this.checkValidity(event.target.value, this.state.controls[controlName].validation),
                touched: true
            }
        };
        this.setState({ controls: updatedControls });
    }

    submitHandler = (event) => {
        event.preventDefault();
        if (!this.state.controls.name.valid) {
            this.setState({ error_message: 'Please enter Name' });
        }
        else if (!this.state.controls.contact.valid) {
            this.setState({ error_message: 'Please enter valid contact of 10 digit' });
        }
        else if (!this.state.controls.email.valid) {
            this.setState({ error_message: 'Please enter valid email' });
        }
        else if (!this.state.controls.username.valid) {
            this.setState({ error_message: 'Please enter Username' });
        }
        else if (!this.state.controls.password.valid) {
            this.setState({ error_message: 'Password minimum 5 character' });
        }
        else {
            localStorage.setItem("userData", JSON.stringify({ name: this.state.controls.name.value, contact: this.state.controls.contact.value, email: this.state.controls.email.value, username: this.state.controls.username.value, password: this.state.controls.password.value }))
            console.log(JSON.parse(localStorage.getItem("userData")));
            if (this.props.match.params.type === 'edit') {
                this.props.history.push('/home');

            } else {
                this.props.history.push('/sign-in');
            }
        }

    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.controls) {
            formElementsArray.push({
                id: key,
                config: this.state.controls[key]
            });
        }

        let form = formElementsArray.map(formElement => (
            <Input
                key={formElement.id}
                elementType={formElement.config.elementType}
                elementConfig={formElement.config.elementConfig}
                value={formElement.config.value}
                label={formElement.config.label}
                invalid={!formElement.config.valid}
                shouldValidate={formElement.config.validation}
                touched={formElement.config.touched}
                changed={(event) => this.inputChangedHandler(event, formElement.id)} />
        ));


        let errorMessage = null;

        if (this.state.error_message) {
            errorMessage = (
                <p style={{ color: 'red' }}>{this.state.error_message}</p>
            );
        }

        return (
            <div>
                {this.props.match.params.type == 'edit' ? <h3>Update Information</h3> : <h3>Register</h3>}
                {errorMessage}
                <form onSubmit={this.submitHandler}>
                    {form}
                    <button type="submit" className="btn btn-primary btn-block">Submit</button>
                    {this.props.match.params.type == 'edit' ? <Link to="/home" className="btn btn-primary btn-block">Back To Home</Link> : <p className="forgot-password text-right">Already exist ? login here ! <Link to="/sign-in">sign In?</Link></p>}
                </form>
            </div >
        );
    }
}