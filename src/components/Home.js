import React, { Component } from "react";
import { Redirect, Link } from 'react-router-dom';

export default class Home extends Component {


    render() {

        if (localStorage.getItem('login') == 'true') {
            return (
                <div>
                    <h5>Hello, {JSON.parse(localStorage.getItem('userData')).name}</h5><span>Your Details</span>
                    <p></p>
                    <p></p>
                    <p>Name : {JSON.parse(localStorage.getItem('userData')).name}</p>
                    <p>Username : {JSON.parse(localStorage.getItem('userData')).username}</p>
                    <p>Email : {JSON.parse(localStorage.getItem('userData')).email}</p>
                    <p>Password : {JSON.parse(localStorage.getItem('userData')).password}</p>
                    <p>Phone : {JSON.parse(localStorage.getItem('userData')).contact}</p>
                    <Link to="sign-up/edit" type="submit" className="btn btn-primary btn-block">Edit Information</Link>
                </div >
            )
        } else {
            return <Redirect to="/" />
        }


    }
}